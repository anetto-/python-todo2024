import json
import pytest

from src.TodoJournal import TodoJournal


def test_init():
    """Проверка корректности инициализации TodoJournal"""
    # TODO проблема. чтобы сейчас протестировать нужно знать путь до тудушки, ее имя.
    expected_entries = []
    expected_name = "test_todo"

    todo = TodoJournal("data/test_todo", expected_name)
    entries = todo.entries
    name = todo.name

    assert entries == expected_entries
    assert name == expected_name


def test_create_journal(tmpdir):
    todo_filename = "test_todo"
    todo = tmpdir.join(todo_filename)

    expected_todo = json.dumps(
        {
            "name": "test",
            "todos": []
        },
        indent=4)

    TodoJournal.create(todo, "test")

    assert expected_todo == todo.read()


def test_add_entry(tmpdir):
    todo_filename = "test_todo"
    todo = tmpdir.join(todo_filename)

    expected_todo = json.dumps(
        {
            "name": "test",
            "todos": ["go for a milk"]
        },
        indent=4,
        ensure_ascii=False, )

    TodoJournal.create(todo, "test")
    todo_jrnl = TodoJournal(todo, "test")
    todo_jrnl.add_entry("go for a milk")

    print(todo.read())
    assert expected_todo == todo.read()


@pytest.fixture()
def todo_journal_with_3_entries(tmpdir):
    todo_filename = "test_todo"
    todo_path = tmpdir.join(todo_filename)
    with open(todo_path, "w") as f:
        json.dump(
            {
                "name": "test",
                "todos": ["first entry", "second_entry", "third entry"]
            },
            f,
            indent=4,
            ensure_ascii=False, )
    return todo_path


@pytest.fixture()
def todo_json_after_remove_second_entry():
    return json.dumps(
        {
            "name": "test",
            "todos": ["first entry", "third entry"]
        },
        indent=4,
        ensure_ascii=False, )


@pytest.fixture()
def todo_object_with_with_3_entries(todo_journal_with_3_entries):
    return TodoJournal(todo_journal_with_3_entries, "test")

# def test_remove_entry(todo_object_with_with_3_entries, todo_json_after_remove_second_entry):
#     expected_todo_json_after_remove_second_entry = todo_json_after_remove_second_entry
#
#     #print(todo_object_with_with_3_entries)
#     todo_object_with_with_3_entries.remove_entry(1)
#
#     assert expected_todo_json_after_remove_second_entry == todo_object_with_with_3_entries.path_todo.read()
