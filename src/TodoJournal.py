import json
import sys

# snake_case
# CamelCase
# Idiot_Case <- dont use

class TodoJournal:
    def __init__(self, path_todo, name):
        print("Initted")
        self.path_todo = path_todo
        self.name = name
        self.entries = []
        # print(self)
        # print(dir(self))
        TodoJournal.create(self.path_todo, self.name)

    # old non-static create
    # def create(self):
    #     print("in create")
    #     new_data = {"name": self.name}
    #     self._update(new_data)

    @staticmethod
    def create(filename, name):
        with open(filename, "w", encoding='utf-8') as todo_file:
            json.dump(
                {"name": name, "todos": []},
                todo_file,
                sort_keys=True,
                indent=4,
                ensure_ascii=False,
            )

    def add_entry(self, new_todo):
        """
        Adding entry to todo list
        :param new_todo: title of todo to be done
        :return: None

        Side Effect: modify self.path_todo file
        """
        data = self._parse()

        name = data["name"]
        todos = data.get("todos", [])

        todos.append(new_todo)

        new_data = {
            "name": name,
            "todos": todos,
        }

        self._update(new_data)

    def remove_entry(self, index):
        data = self._parse()
        name = data["name"]
        todos = data["todos"]

        todos.remove(todos[index])

        new_data = {
            "name": name,
            "todos": todos,
        }

        self._update(new_data)

    def __private_update(self):
        print("like a private")

    def _update(self, new_data):
        with open(self.path_todo, "w", encoding="utf-8") as todo_file:
            json.dump(
                new_data,
                todo_file,
                sort_keys=True,
                indent=4,
            )

    def _parse(self):
        try:
            with open(self.path_todo, 'r') as todo_file:
                data = json.load(todo_file)
            return data
        except FileNotFoundError as error:
            print(f"{error}")
            # или свое исключение
            print(f"Не существует такой тудушки: {self.path_todo}")
            sys.exit(1)

def my_generator():
    values = [1, 2, 3]
    for item in values:
        yield item

