import sys

from src.TodoJournal import my_generator, TodoJournal

# файл args.py
import argparse


def parse_args(args):
    parser = argparse.ArgumentParser()
    composing = parser.add_argument_group("Writing new todos",
                                          "чтобы добавить новую запись в туду лист необходимо выполнить Х")
    composing.add_argument("text", metavar="", nargs="*")
    return parser.parse_intermixed_args(args)


# файл todo.py
# from src.TodoJournal import TodoJournal
def run(args):
    path_todo = "src/mytodo.json"
    todo_name = "anetto_todo"
    try:
        todo = TodoJournal(path_todo, todo_name)
    except IsADirectoryError as err:
        print(err)
        # просто перепородить исключение
        # raise

        # заменить исключение на другое, в том числе кастомное (своё)
        raise ValueError()

    if args.text:
        print(f"parsed args: {args.text}")
        # todo.add_entry(args.text)
        raw_text = ''.join(args.text)
        todo.add_entry(raw_text)


def main():
    print("Hello world")

    # example of generator usage
    res = my_generator()
    print(f"generator printed: {res}")
    print(f"generator type: {type(res)}")
    print(f"generator dir: {dir(res)}")
    print(f"generator next: {next(res)}")
    print(f"generator next: {next(res)}")


    try:
        # 1. Спарсить аргументы командной
        cli_args = sys.argv[1:]
        # 2. обработать аргументы командной строки
        args = parse_args(cli_args)
        # 2. вызвать соответствующие функции
        return run(args)
    except Exception as e:
        # TODO Создать свое исключение для обработки такой ситуцации (будем обсуждать на следующих пз)
        print(e)
        return 1

    # new_todo = "walk the dog"
    # # without class
    # # add_todo(path_todo, new_todo)
    # todo.add_entry(new_todo)
    #
    # todo.add_entry("task1")
    # todo.add_entry("task2")
    # todo.add_entry("task3")
    #
    # # without class
    # # remove_todo(path_todo, 1)
    # todo.remove_entry(1)

    # print(dir(todo))

    # pylint dont like it
    # todo._update({})

    # example of access to __private method
    # todo._TodoJournal__private_update()

    # without class
    # create_todo_list(path_todo, todo_name)
    # todo.create()




if __name__ == '__main__':
    main()
